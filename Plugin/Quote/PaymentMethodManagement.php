<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\Quote;

use Magento\Quote\Model\PaymentMethodManagement as Subject;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use Resursbank\Core\Model\PaymentMethodRepository;
use Resursbank\Core\Helper\PaymentMethods;
use Resursbank\Rco\Helper\Config;

class PaymentMethodManagement
{
    /**
     * @param CartRepositoryInterface $quoteRepository
     * @param PaymentMethodRepository $methodRepository
     * @param PaymentMethods $paymentMethods
     * @param Config $config
     */
    public function __construct(
        private readonly CartRepositoryInterface $quoteRepository,
        private readonly PaymentMethodRepository $methodRepository,
        private readonly PaymentMethods $paymentMethods,
        private readonly Config $config
    ) {
    }

    /**
     * Overrides payment method with "free" to support zero-sum orders.
     *
     * Overrides payment method with "resursbank_default" to support payment
     * methods which haven't been synced yet (i.e. exists at Resurs Bank thus
     * appears within the iframe, but has no counterpart in Magento thus causes
     * an Exception when applied on the Quote).
     *
     * @param Subject $subject
     * @param mixed $cartId
     * @param PaymentInterface $method
     * @return array<mixed, PaymentInterface>
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function beforeSet(
        Subject $subject,
        $cartId,
        PaymentInterface $method
    ): array {
        $code = (string) $method->getMethod();

        /** @var Quote $quote */
        $quote = $this->quoteRepository->get($cartId);

        if ($this->config->isActive($quote->getStore()->getCode()) &&
            $this->paymentMethods->isResursBankMethod($code)
        ) {
            if ((float) $quote->getGrandTotal() === 0.0) {
                $method->setMethod('free');
            } elseif ($code !== 'resursbank_default') {
                $model = $this->methodRepository->getByCode($code);

                if (!$model->getActive()) {
                    $method->setMethod('resursbank_default');
                }
            }
        }

        return [$cartId, $method];
    }
}
