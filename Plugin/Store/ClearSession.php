<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\Store;

use Exception;
use Magento\Store\Controller\Store\Redirect;
use Magento\Checkout\Controller\Onepage\Failure;
use Magento\Checkout\Controller\Onepage\Success;
use Magento\Framework\Controller\Result\Redirect as ResultRedirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Rco\Helper\Config;
use Resursbank\Rco\Helper\Log;
use Resursbank\Rco\Helper\Session;

/**
 * Clear this module's own session data after a successful order placement. The
 * checkout session will not be touched.
 */
class ClearSession
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @param Log $log
     * @param Config $config
     * @param Session $session
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Log $log,
        Config $config,
        Session $session,
        StoreManagerInterface $storeManager
    ) {
        $this->log = $log;
        $this->config = $config;
        $this->session = $session;
        $this->storeManager = $storeManager;
    }

    /**
     * Intercept calls to the execute method.
     *
     * @param Success|Failure|Redirect $subject
     * @param ResultInterface|ResultRedirect|Page $result
     * @return ResultInterface|ResultRedirect|Page
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterExecute(
        $subject,
        $result
    ) {
        try {
            $storeCode = $this->storeManager->getStore()->getCode();

            if ($this->config->isActive($storeCode)) {
                $this->session->clear();
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
