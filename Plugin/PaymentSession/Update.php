<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\PaymentSession;

use Exception;
use Magento\Quote\Model\Quote;
use Magento\Store\Model\ScopeInterface;
use Resursbank\Core\Helper\Api\Credentials;
use Resursbank\Rco\Helper\Log;
use Resursbank\Rco\Helper\Session;

class Update
{
    /**
     * @var Credentials
     */
    private Credentials $credentials;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * Init constructor.
     *
     * @param Credentials $credentials
     * @param Log $log
     * @param Session $session
     */
    public function __construct(
        Credentials $credentials,
        Log $log,
        Session $session
    ) {
        $this->credentials = $credentials;
        $this->log = $log;
        $this->session = $session;
    }

    /**
     * Update Resurs Bank payment session after the quote has been saved.
     *
     * @param Quote $subject
     * @param Quote $result
     * @return Quote
     * @throws Exception
     */
    public function afterAfterSave(
        Quote $subject,
        Quote $result
    ): Quote {
        try {
            if ($this->isEnabled($subject)) {
                $this->session->update(
                    $this->credentials->resolveFromConfig(
                        $subject->getStore()->getCode(),
                        ScopeInterface::SCOPE_STORES
                    )
                );
            }
        } catch (Exception $e) {
            $this->log->exception($e);

            throw $e;
        }

        return $result;
    }

    /**
     * Check if this plugin should execute.
     *
     * @param Quote $subject
     * @return bool
     */
    private function isEnabled(
        Quote $subject
    ): bool {
        return $this->session->isInitialized() &&
            $subject->getIsActive();
    }
}
