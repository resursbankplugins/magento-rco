<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\PaymentSession;

use Exception;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Helper\Api as Subject;
use Resursbank\RBEcomPHP\RESURS_FLOW_TYPES;
use Resursbank\RBEcomPHP\ResursBank;
use Resursbank\Rco\Helper\Config;
use Resursbank\Rco\Helper\Log;

class FlowType
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param Log $log
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Log $log,
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        $this->log = $log;
        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * Set the flow type of Ecom instance to Resurs Checkout.
     *
     * @param Subject $subject
     * @param ResursBank $result
     * @return ResursBank
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetConnection(
        Subject $subject,
        ResursBank $result
    ): ResursBank {
        try {
            $storeCode = $this->storeManager->getStore()->getCode();

            if ($this->config->isActive($storeCode)) {
                $result->setPreferredPaymentFlowService(
                    RESURS_FLOW_TYPES::RESURS_CHECKOUT
                );
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
