<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Test\Unit\Plugin\Helper;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Helper\Api as CoreApi;
use Resursbank\Core\Helper\Version;
use Resursbank\Rco\Plugin\Helper\Api;

class ApiTest extends TestCase
{
    /**
     * @var Version|MockObject
     */
    private $versionMock;

    /**
     * @var Api
     */
    private Api $api;

    /**
     * Main set up method
     */
    public function setUp(): void
    {
        $this->versionMock = $this->createMock(Version::class);
        $this->api = new Api($this->versionMock);
    }

    /**
     * Assert that afterGetUserAgent return the correct string.
     */
    public function testAfterGetUserAgent(): void
    {
        /** @phpstan-ignore-next-line Undefined method. */
        $this->versionMock
            ->expects(self::once())
            ->method('getComposerVersion')
            ->with('Resursbank_Rco')
            ->willReturn('1.0.0');
        $coreApiMock = $this->createMock(CoreApi::class);
        $this->assertEquals(
            'Magento 2 | Resursbank_Core 1.0.0 | Resursbank_Rco 1.0.0',
            $this->api->afterGetUserAgent($coreApiMock, 'Magento 2 | Resursbank_Core 1.0.0')
        );
    }
}
