<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Observer\Customer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Resursbank\Rco\Helper\Session;

/**
 * Clear module specific session values when customer logs out.
 */
class Logout implements ObserverInterface
{
    /**
     * @var Session
     */
    private Session $paymentSession;

    /**
     * @param Session $paymentSession
     */
    public function __construct(
        Session $paymentSession
    ) {
        $this->paymentSession = $paymentSession;
    }

    /**
     * Clear payment session when client logs out.
     *
     * @param Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(Observer $observer): void
    {
        $this->paymentSession->clear();
    }
}
