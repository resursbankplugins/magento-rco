<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Controller\Checkout;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\Result\Json;
use Resursbank\Rco\Helper\Log;
use Resursbank\Rco\Helper\Session;
use Resursbank\Rco\Exception\PaymentSessionException;
use Resursbank\Core\Helper\Order as OrderHelper;
use Resursbank\Core\Helper\Cart as CartHelper;

/**
 * This controller will execute when you receive credit denied within the
 * iframe.
 */
class Denied implements HttpGetActionInterface
{
    /**
     * @var Context
     */
    private Context $context;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;

    /**
     * @var OrderHelper
     */
    private OrderHelper $orderHelper;

    /**
     * @var CartHelper
     */
    private CartHelper $cartHelper;

    /**
     * @param Context $context
     * @param CheckoutSession $checkoutSession
     * @param Log $log
     * @param Session $session
     * @param OrderHelper $orderHelper
     * @param CartHelper $cartHelper
     */
    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession,
        Log $log,
        Session $session,
        OrderHelper $orderHelper,
        CartHelper $cartHelper
    ) {
        $this->context = $context;
        $this->log = $log;
        $this->session = $session;
        $this->checkoutSession = $checkoutSession;
        $this->orderHelper = $orderHelper;
        $this->cartHelper = $cartHelper;
    }

    /**
     * @inheritDoc
     *
     * Apply the "Credit Denied" status on the active order and rebuild the
     * cart in preparation of the next order attempt.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $data = [];

        try {
            if (!$this->session->isInitialized()) {
                throw new PaymentSessionException(
                    __('Checkout session not initialized.')
                );
            }

            // This is needed to free up cart contents as being salable
            $this->orderHelper->cancelOrder(order: $this->checkoutSession->getLastRealOrder());

            $this->orderHelper->setCreditDeniedStatus(
                $this->checkoutSession->getLastRealOrder()
            );

            $this->cartHelper->rebuildCart(
                $this->checkoutSession->getLastRealOrder()
            );
        } catch (PaymentSessionException $e) {
            $data['message'] = $e->getMessage();
        } catch (Exception $e) {
            $data['message'] = __('An unknown error occurred');
            $this->log->exception($e);
        }

        /** @var Json $response */
        $response = $this->context->getResultFactory()->create(
            ResultFactory::TYPE_JSON
        );

        $response->setData($data);
        return $response;
    }
}
