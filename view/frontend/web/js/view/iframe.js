/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'jquery',
        'underscore',
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/action/set-shipping-information',
        'Resursbank_Rco/js/model/session',
        'Resursbank_Rco/js/lib/customer',
        'Resursbank_Rco/js/lib/shipping-rates',
        'Resursbank_Rco/js/lib/payment',
        'Resursbank_Rco/js/lib/checkout',
        'Resursbank_Rco/js/action/checkout',
        'Resursbank_Rco/js/model/checkout'
    ],
    /**
     * @param $
     * @param _
     * @param ko
     * @param Component
     * @param ShippingService
     * @param Quote
     * @param Totals
     * @param SetShippingInformation
     * @param {Rco.Model.Session} Session
     * @param {Rco.Lib.Customer} Customer
     * @param {Rco.Lib.ShippingRates} ShippingRates
     * @param {Rco.Lib.Payment} Payment
     * @param {Rco.Lib.Checkout} Checkout
     * @param {Rco.Action.Checkout} CheckoutAction
     * @param {Rco.Model.Checkout} Model
     * @returns {*}
     */
    function (
        $,
        _,
        ko,
        Component,
        ShippingService,
        Quote,
        Totals,
        SetShippingInformation,
        Session,
        Customer,
        ShippingRates,
        Payment,
        Checkout,
        CheckoutAction,
        Model
    ) {
        'use strict';

        /**
         * Iframe interface.
         */
        var $R = window.$ResursCheckout;

        /**
         * An alert message should be displayed when shipping rates are changed,
         * except when the rates initial load when you enter the checkout page,
         * and when the list is cleared at order placement.
         *
         * @type {boolean}
         */
        var displayShippingRatesAlert = false;

        /**
         * @type {Rco.Lib.ShippingRates.Rate[]}
         */
        var previousShippingRates = [];

        /**
         * Event handler for when customer address changes in the iframe.
         *
         * @param {Rco.Lib.Customer.ChangeEvent} event
         */
        function onCustomerChange(event) {
            var oldAddress = Model.address();
            var newAddress = Customer.toAddressData(event);

            if (
                Customer.isValid(newAddress) &&
                (
                    !oldAddress ||
                    !Customer.sameAddressData(newAddress, oldAddress)
                )
            ) {
                Model.address(newAddress);
                Customer.selectAddress(newAddress);
            }
        }

        /**
         * Event handler for when the payment method in the iframe changes.
         *
         * @param {Rco.Lib.Payment.ChangeEvent} event
         */
        function onPaymentChange(event) {
            var method = Payment.toMethod(event);

            if (!Payment.same(method, Model.paymentMethod()) &&
                Payment.select(method)
            ) {
                CheckoutAction.setPaymentMethod(method);
            }
        }

        /**
         * Event handler for when credit application is denied.
         *
         * @param {Rco.Lib.Payment.Failed} event
         */
        function onPaymentFail(event) {
            /**
             * NOTE: At the time of writing there is no other way to confirm
             * that the error received within the iframe was "credit denied".
             * This solution has been confirmed by the RCO development team.
             */
            if (event.hasOwnProperty('url') ||
                typeof event.url === 'undefined'
            ) {
                /**
                 * Shipping methods are hidden at submissions. Displaying them
                 * again counts as the list changing, but we don't need any
                 * message here since the same methods are still within the
                 * list.
                 */
                displayShippingRatesAlert = false;

                showShippingMethods();

                $.ajax({
                    url: Checkout.buildUrl('checkout/denied'),
                    type: 'GET',
                    data: {},
                    contentType: 'application/json'
                }).done(function (response) {
                    if (response.hasOwnProperty('message')) {
                        alert(response.message);
                    }
                });
            }
        }

        /**
         * Event handler for when the iframe purchase button has been pushed.
         */
        function onSubmit() {
            /**
             * We hide our shipping methods block to ensure the client cannot
             * manipulate the data during the submission process. This counts
             * as the list changing, but we do not need a message since the
             * methods within the list are the same.
             */
            displayShippingRatesAlert = false;

            hideShippingMethods();
            hideDiscountCodeBlock();

            CheckoutAction.checkStatus()
                .done(onStatusSuccess);
        }

        /**
         * Hide shipping method related blocks (note that each block must be
         * specified separately for this to function correctly).
         */
        function hideShippingMethods()
        {
            $('#shipping').hide();
            $('#opc-shipping_method').hide();
            $('#resursbank-rco-shipping-method-title').hide();
            $('#resursbank-rco-sign-in').hide();
        }

        /**
         * Show shipping method related blocks (note that each block must be
         * specified separately for this to function correctly).
         */
        function showShippingMethods()
        {
            $('#shipping').show();
            $('#opc-shipping_method').show();
            $('#resursbank-rco-shipping-method-title').show();
            $('#resursbank-rco-sign-in').show();
        }

        /**
         * Hide the HTML elements for the discount code section.
         */
        function hideDiscountCodeBlock()
        {
            $('.resursbank-rco-discount.checkout-payment-method').hide();
        }

        /**
         * Show the HTML elements for the discount code section.
         */
        function showDiscountCodeBlock()
        {
            $('.resursbank-rco-discount.checkout-payment-method').show();
        }

        /**
         * Handler for when the status check of the checkout process was
         * successful.
         *
         * @param {Rco.Lib.Checkout.StatusResponse} response
         */
        function onStatusSuccess(response) {
            if (response.isSessionValid) {
                CheckoutAction.placeOrder(Model.paymentMethod())
                    .done(function () {
                        if (response.isGrandTotalZero) {
                            // We do not want the iframe to register an order
                            // if the grand total is zero.
                            Checkout.redirectToOrderSuccess();
                        } else {
                            // Let the iframe continue.
                            window.$ResursCheckout.release();
                        }
                    })
                    .fail(function () {
                        Checkout.createOrderPlacementErrorAlert();
                    });
            } else {
                Checkout.createInvalidSessionAlert(
                    response.invalidSessionError
                );
            }
        }

        /**
         * KO subscriber function for when the list of available shipping
         * methods gets updated.
         */
        function onShippingRatesUpdate() {
            /**
             * @type {Rco.Lib.ShippingRates.Rate[]}
             */
            var newShippingRates = ShippingService.getShippingRates()();
            var hasChanged = ShippingRates.hasChanged(
                previousShippingRates,
                newShippingRates
            );

            if (hasChanged) {
                previousShippingRates = newShippingRates;

                if (displayShippingRatesAlert) {
                    ShippingRates.createUpdatedAlert();
                } else {
                    displayShippingRatesAlert = true;
                }
            }
        }

        /**
         * KO subscriber function for when the selected shipping method
         * changes.
         */
        function onShippingMethodUpdate() {
            sendShippingInfo();
        }

        /**
         * Send shipping information to the server.
         */
        function sendShippingInfo() {
            $R.lock();
            SetShippingInformation().always(function () {
                $R.unlock()
            });
        }

        return Component.extend({
            defaults: {
                template: 'Resursbank_Rco/iframe'
            },

            initialize: function () {
                this._super();

                showShippingMethods();
                showDiscountCodeBlock();

                Quote.shippingMethod.subscribe(onShippingMethodUpdate);

                if (Session.isAlertShippingMethodUpdates()) {
                    ShippingService.getShippingRates()
                        .subscribe(onShippingRatesUpdate);
                }
            },

            /**
             * Initializes the iframe element. We cannot initialize the iframe
             * in the component's init function, the container has not been
             * rendered at that point.
             */
            initIframe: function () {
                $R.create({
                    containerId: 'resursbank-rco-iframe-container',
                    baseUrl: Session.getUrl(),
                    paymentSessionId: Session.getId(),
                    hold: true
                });

                $R.onCustomerChange(onCustomerChange);
                $R.onPaymentChange(onPaymentChange);
                $R.onPaymentFail(onPaymentFail);
                $R.onSubmit(onSubmit);

                // Totals.isLoading.subscribe(function (state) {
                //     if (state) {
                //         $R.lock();
                //     } else {
                //         $R.unlock();
                //     }
                // });
            }
        });
    }
);
