/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'jquery',
        'uiComponent',
        'Magento_Checkout/js/model/quote'
    ],
    /**
     * @param $
     * @param Component
     * @param Quote
     * @returns {*}
     */
    function (
        $,
        Component,
        Quote
    ) {
        'use strict';

        /**
         * @typedef Rco.ShippingMethod
         * @property {string} carrier_code
         * @property {string} carrier_title
         * @property {string} method_code
         * @property {string} method_title
         * @property {string} error_message
         * @property {number} amount
         * @property {number} price_excl_tax
         * @property {number} price_incl_tax
         * @property {boolean} available
         */

        /**
         * KO subscriber function for when the selected shipping method
         * changes.
         *
         * @param {Rco.ShippingMethod} method
         */
        function onShippingMethodUpdate(method) {
            selectShippingMethod(method.method_code);
        }

        /**
         * Sets the selected state of all shipping methods.
         *
         * You can pass in the data from a shipping method to select that
         * method and deselect the others.
         *
         * @param {string} methodCode
         */
        function selectShippingMethod(methodCode) {
            $('#checkout-shipping-method-load table tbody tr input')
                .each(function (i, el) {
                    if (el.value.indexOf(methodCode) > -1) {
                        $(el)
                            .closest('tr')
                            .addClass('resursbank-rco-selected');
                    } else {
                        $(el)
                            .closest('tr')
                            .removeClass('resursbank-rco-selected');
                    }
                });
        }

        return Component.extend({
            initialize: function () {
                this._super();

                Quote.shippingMethod.subscribe(onShippingMethodUpdate);
            }
        });
    }
);
