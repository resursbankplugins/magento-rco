/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define(
    [
        'jquery',
        'Magento_Checkout/js/action/place-order',
        'Magento_Ui/js/model/messages',
        'Resursbank_Rco/js/lib/payment',
        'Resursbank_Rco/js/lib/checkout',
        'Resursbank_Rco/js/model/checkout'
    ],
    /**
     * @param {*} $
     * @param {*} PlaceOrderAction
     * @param {*} Messages
     * @param {Rco.Lib.Payment} Payment
     * @param {Rco.Lib.Checkout} Checkout
     * @param {Rco.Model.Checkout} Model
     * @returns {Readonly<Rco.Action.Checkout>}
     */
    function (
        $,
        PlaceOrderAction,
        Messages,
        Payment,
        Checkout,
        Model
    ) {
        /**
         * @constant
         * @namespace Rco.Action.Checkout
         */
        var EXPORT = {
            /**
             * Set the selected payment method of the iframe.
             *
             * @param {Rco.Lib.Payment.Method} value
             * @return {Readonly<Rco.Action.Checkout>}
             */
            setPaymentMethod: function (value) {
                Model.paymentMethod(value);

                return EXPORT;
            },

            /**
             * Check the status of the payment session and the grand total.
             *
             * @returns {Deferred} Gets {@link Rco.Lib.Checkout.StatusResponse}
             *  as an response object.
             */
            checkStatus: function () {
                return $.ajax({
                    url: Checkout.buildUrl('checkout/status'),
                    type: 'GET',
                    data: {},
                    contentType: 'application/json'
                });
            },

            /**
             * Places the order in Magento.
             *
             * @param {Rco.Lib.Payment.Method} method -
             *  {@link Rco.Lib.Payment.Method}
             * @return {Deferred}
             */
            placeOrder: function (method) {
                var result = $.Deferred();

                if (Payment.isMethod(method) &&
                    Payment.validateMethod(method)
                ) {
                    PlaceOrderAction(
                        Payment.toRequestData(method),
                        new Messages()
                    ).done(function () {
                        result.resolve();
                    }).fail(function () {
                        result.reject();
                    });
                }

                return result;
            }
        };

        return Object.freeze(EXPORT);
    }
);
