/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'jquery',
        'mage/url',
        'Magento_Ui/js/modal/alert'
    ],
    /**
     * @param $
     * @param Url
     * @param UiAlert
     * @returns {Readonly<Rco.Lib.Checkout>}
     */
    function (
        $,
        Url,
        UiAlert
    ) {
        /**
         * @typedef {object} Rco.Lib.Checkout.StatusResponse
         * @property {boolean} isSessionValid
         * @property {boolean} isGrandTotalZero
         * @property {string} invalidSessionError
         */

        /**
         * @constant
         * @namespace Rco.Lib.Checkout
         */
        var EXPORT = {
            /**
             * Creates and displays an alert message to the customer that will
             * inform them that the payment session of the checkout process has
             * expired.
             *
             * @note The page will reload when the alert is dismissed.
             *
             * @param {string} message - Can be specified, however, the message
             *  typically comes from the server after doing a status check.
             * @returns {Readonly<Rco.Lib.Checkout>}
             */
            createInvalidSessionAlert: function (message) {
                UiAlert({
                    title: '',
                    content: EXPORT.createAlertContent([message]),
                    clickableOverlay: false,
                    focus: 'none',
                    autoOpen: false,
                    modalClass: 'resursbank-rco-alert',
                    buttons: [{
                        text: $.mage.__('OK'),
                        class: 'action primary accept',

                        click: function () {
                            this.closeModal(true);
                            window.location.reload();
                        }
                    }]
                });

                return EXPORT;
            },

            /**
             * Creates and displays an alert message to the customer that will
             * inform them that the order could not be placed.
             *
             * @note The page will reload when the alert is dismissed.
             *
             * @returns {Readonly<Rco.Lib.Checkout>}
             */
            createOrderPlacementErrorAlert: function () {
                UiAlert({
                    title: '',
                    content: EXPORT.createAlertContent([
                        'An error occurred when placing the order. The ' +
                        'page will reload after this message has been ' +
                        'dismissed. Please make sure you have entered ' +
                        'all the necessary information and try again.'
                    ]),
                    clickableOverlay: false,
                    focus: 'none',
                    autoOpen: false,
                    modalClass: 'resursbank-rco-alert',
                    buttons: [{
                        text: $.mage.__('OK'),
                        class: 'action primary accept',

                        click: function () {
                            this.closeModal(true);
                            window.location.reload();
                        }
                    }]
                });

                return EXPORT;
            },


            createAlertContent: function (messages) {
                return $(document.createElement('div'))
                    .addClass('resursbank-rco-alert-content')
                    .append(EXPORT.createAlertLogo())
                    .append(EXPORT.createAlertInfo(messages));
            },


            createAlertLogo: function () {
                return $(document.createElement('div'))
                    .addClass('resursbank-rco-alert-logo');
            },


            createAlertInfo: function (messages) {
                var info = $(document.createElement('div'))
                    .addClass('resursbank-rco-alert-info');

                messages.forEach(function (message) {
                    info.append(EXPORT.createAlertInfoParagraph(message));
                });

                return info;
            },


            createAlertInfoParagraph: function (message) {
                return $(document.createElement('p'))
                    .text($.mage.__(message));
            },

            /**
             * Redirects the customer to the order success page.
             *
             * @returns {boolean}
             */
            redirectToOrderSuccess: function () {
                window.location.replace(
                    window.checkoutConfig.defaultSuccessPageUrl
                );

                return false;
            },

            /**
             * Builds a URL to connect to a controller in the Rco module.
             *
             * @param {string} path - Path to a controller action. Should not
             *  start with a "/".
             * @returns {string} URL to a controller in the Rco module.
             */
            buildUrl: function (path) {
                return Url.build('resursbank_rco/' + path);
            }
        };

        return Object.freeze(EXPORT);
    }
);
