/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define(
    [
        'Magento_Checkout/js/model/new-customer-address',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer'
    ],
    /**
     * @param NewCustomerAddress
     * @param Quote
     * @param Customer
     * @returns {Readonly<Rco.Lib.Customer>}
     */
    function (
        NewCustomerAddress,
        Quote,
        Customer
    ) {
        /**
         * @typedef {object} Rco.Lib.Customer.Address
         * @property {string|null} firstName
         * @property {string|null} lastName
         * @property {string|null} city
         * @property {string|null} postalCode
         * @property {string|null} addressRow1
         * @property {string|null} addressRow2
         */

        /**
         * @typedef {object} Rco.Lib.Customer.ChangeEvent
         * @property {Rco.Lib.Customer.Address} billingAddress
         * @property {Rco.Lib.Customer.Address} deliveryAddress
         * @property {string} email
         * @property {string} phone
         * @property {string} source
         * @property {number} ts
         * @property {string} type
         */

        /**
         * @typedef {object} Rco.Lib.Customer.AddressData
         * @property {Rco.Lib.Customer.Address} billing
         * @property {Rco.Lib.Customer.Address} shipping
         * @property {string} email
         * @property {string} phone
         */

        /**
         * @namespace Rco.Lib.Customer
         * @constant
         */
        var EXPORT = {
            /**
             * Converts an iframe address object to an address object that the
             * Magento quote accepts.
             *
             * @param {Rco.Lib.Customer.Address} addressObj
             * @param {string} email
             * @param {string} phone
             * @return {object} Magento quote address.
             */
            toQuoteAddress: function (addressObj, email, phone) {
                var newAddress = NewCustomerAddress({});

                newAddress.firstname = addressObj.firstName === null ?
                    newAddress.firstname :
                    addressObj.firstName;

                newAddress.lastname = addressObj.lastName === null ?
                    newAddress.lastname :
                    addressObj.lastName;

                newAddress.city = addressObj.city === null ?
                    newAddress.city :
                    addressObj.city;

                newAddress.postcode = addressObj.postalCode === null ?
                    newAddress.postcode :
                    addressObj.postalCode;

                if (typeof addressObj.addressRow1 === 'string') {
                    newAddress.street = [addressObj.addressRow1];

                    if (typeof addressObj.addressRow2 === 'string') {
                        newAddress.street.push(addressObj.addressRow2);
                    }
                }

                newAddress.email = email;
                newAddress.telephone = phone;

                return newAddress;
            },

            /**
             * Check if an object is a valid customer address object sent from
             * the iframe.
             *
             * A valid object must have values for all of the fields in the
             * {Rco.Lib.Customer.Address} type, except for
             * {addressRow2}, which may be null.
             *
             * @see {Rco.Lib.Customer.Address}
             * @param {Rco.Lib.Customer.Address} addressObj
             */
            isValidAddress: function (addressObj) {
                return Object.keys(addressObj).every(function (key) {
                    return typeof addressObj[key] === 'string' ||
                        (key === 'addressRow2' && addressObj[key] === null);
                });
            },

            /**
             * Check if all customer information is valid. It requires a valid
             * billing address, along with a valid email and phone number.
             *
             * Shipping address is optional, but it will also be validated if
             * the function detects that there's any shipping address
             * information available.
             *
             * @param {Rco.Lib.Customer.AddressData} data
             */
            isValid: function (data) {
                return typeof data.email === 'string' &&
                    typeof data.phone === 'string' &&
                    EXPORT.isValidAddress(data.billing) &&
                    (EXPORT.hasShippingAddress(data) ?
                        EXPORT.isValidAddress(data.shipping) :
                        true
                    );
            },

            /**
             * Whether the customer address includes a shipping address.
             *
             * @param {Rco.Lib.Customer.AddressData} data
             */
            hasShippingAddress: function (data) {
                return Object.keys(data.shipping)
                    .some(function (key) {
                        return typeof data.shipping[key] === 'string';
                    });
            },

            /**
             * Converts a CustomerChangeEvent to an AddressData object.
             *
             * @param {Rco.Lib.Customer.ChangeEvent} event
             * @return {Rco.Lib.Customer.AddressData}
             */
            toAddressData: function (event) {
                return {
                    billing: event.billingAddress,
                    shipping: event.deliveryAddress,
                    phone: event.phone,
                    email: event.email
                };
            },

            /**
             * Compare two AddressData objects to see if they contain the same
             * information.
             *
             * @param {Rco.Lib.Customer.AddressData} address1
             * @param {Rco.Lib.Customer.AddressData} address2
             * @returns {boolean}
             */
            sameAddressData: function (address1, address2) {
                return address1.email === address2.email &&
                    address1.phone === address2.phone &&
                    Object.keys(address1.billing).every(function (key) {
                        return address1.billing[key] === address2.billing[key] &&
                            address1.shipping[key] === address2.shipping[key];
                    });
            },

            /**
             * Sets the address of the quote on the frontend.
             *
             * @note This function will not update the quote on the server, but
             * might set of subscribers to the shippingAddress and
             * billingAddress properties on the frontend quote.
             *
             * @param {Rco.Lib.Customer.AddressData} address
             */
            selectAddress: function (address) {
                var billingAddress;

                if (EXPORT.isValid(address)) {
                    billingAddress = EXPORT.toQuoteAddress(
                        address.billing,
                        address.email,
                        address.phone
                    );

                    Quote.billingAddress(billingAddress);

                    if (!EXPORT.isValidAddress(address.shipping)) {
                        Quote.shippingAddress(billingAddress);
                    } else {
                        Quote.shippingAddress(EXPORT.toQuoteAddress(
                            address.shipping,
                            address.email,
                            address.phone
                        ));
                    }

                    if (!Customer.isLoggedIn()) {
                        Quote.guestEmail = address.email;
                    }
                }
            }
        };

        return Object.freeze(EXPORT);
    }
);
