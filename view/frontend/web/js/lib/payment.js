/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'underscore',
        'Magento_Checkout/js/action/select-payment-method',
        'Resursbank_Rco/js/model/session'
    ],
    /**
     * @param _
     * @param SelectPaymentMethod
     * @param {Rco.Model.Session} Session
     * @return {Readonly<Rco.Lib.Payment>}
     */
    function (
        _,
        SelectPaymentMethod,
        Session
    ) {
        /**
         * @typedef {object} Rco.Lib.Payment.ChangeEvent
         * @property {number|null} fee
         * @property {string|null} id
         * @property {string|null} method
         * @property {string|null} source
         * @property {number|null} ts
         * @property {string|null} type
         */

        /**
         * @typedef {object} Rco.Lib.Payment.Method
         * @property {number} fee
         * @property {string} id
         * @property {string} method
         */

        /**
         * @see vendor/magento/module-quote/Api/Data/PaymentInterface
         * @typedef {object} Rco.Lib.Payment.RequestData
         * @property {number|null} po_number
         * @property {string|null} method
         * @property {string|null} additional_data
         */

        /**
         * @namespace Rco.Lib.Payment
         * @constant
         */
        var EXPORT = {
            /**
             * Takes a {@link Rco.Lib.Payment.ChangeEvent} and turns it into a
             * {@link Rco.Lib.Payment.Method}
             *
             * @param {Rco.Lib.Payment.ChangeEvent} event
             * @return {Rco.Lib.Payment.Method}
             */
            toMethod: function (event) {
                return {
                    fee: typeof event.fee === 'number' ? event.fee : 0,
                    id: typeof event.id === 'string' ? event.id : '',
                    method: typeof event.method === 'string' ?
                        event.method :
                        ''
                };
            },

            /**
             * Convert iframe payment method into data that is expected when
             * applying payment information in a request to the server.
             *
             * @param {Rco.Lib.Payment.Method} method
             * @returns {Rco.Lib.Payment.RequestData}
             */
            toRequestData: function (method) {
                return {
                    method: EXPORT.toMethodCode(method),
                    po_number: null,
                    additional_data: null
                };
            },

            /**
             * Takes a {@link Rco.Lib.Payment.Method} and returns the code for
             * the method so that it can be located in the database.
             *
             * @param {Rco.Lib.Payment.Method} method
             */
            toMethodCode: function (method) {
                return 'resursbank_' +
                    method.id.toLowerCase() + '_' +
                    Session.getActiveUsername() + '_' +
                    Session.getEnvironment()
            },

            /**
             * Takes a payment method from the iframe and selects it on the
             * quote on the frontend.
             *
             * @param {Rco.Lib.Payment.Method} method
             * @return {boolean} True, if the method was selected.
             */
            select: function (method) {
                var result = false;

                if (EXPORT.validateMethod(method)) {
                    SelectPaymentMethod(EXPORT.toRequestData(method));

                    result = true;
                }

                return result;
            },

            /**
             * Check if two payment methods are the same.
             * 
             * @param {Rco.Lib.Payment.Method} method1
             * @param {Rco.Lib.Payment.Method} method2
             * @return {boolean}
             */
            same: function (method1, method2) {
                return method1 && method2 && method1.id === method2.id;
            },

            /**
             * Validates the information on a payment method that was sent
             * from the iframe.
             *
             * @param {Rco.Lib.Payment.Method} method
             */
            validateMethod: function (method) {
                return typeof method.method === 'string' &&
                    typeof method.id === 'string' &&
                    method.method !== '' &&
                    method.id !== '';
            },

            /**
             * Checks if an object is a {@link Rco.Lib.Payment.Method}.
             *
             * @param {*} value
             * @returns {boolean}
             */
            isMethod: function (value) {
                return _.isObject(value) &&
                    value.hasOwnProperty('method') &&
                    value.hasOwnProperty('id') &&
                    value.hasOwnProperty('fee') &&
                    typeof value.method === 'string' &&
                    typeof value.id === 'string' &&
                    typeof value.fee === 'number';
            }
        };

        return Object.freeze(EXPORT);
    }
);
