<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\ViewModel\Frontend;

use Exception;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Exception\SessionDataException;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Rco\Helper\Config;
use Resursbank\Rco\Helper\Log;
use Resursbank\Rco\Helper\Session as SessionModel;

class Session implements ArgumentInterface
{
    /**
     * @var SessionModel
     */
    private SessionModel $session;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var CoreConfig
     */
    private CoreConfig $coreConfig;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param SessionModel $session
     * @param Log $log
     * @param CoreConfig $coreConfig
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     */
    public function __construct(
        SessionModel $session,
        Log $log,
        CoreConfig $coreConfig,
        StoreManagerInterface $storeManager,
        Config $config
    ) {
        $this->session = $session;
        $this->log = $log;
        $this->coreConfig = $coreConfig;
        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * Returns the iframe lib script tag. Empty string if it doesn't exist.
     *
     * @return string
     */
    public function getIframeLibScriptTag(): string
    {
        $result = '';

        try {
            $result = $this->session->getIframeLibScriptTag();
        } catch (SessionDataException $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Returns the iframe URL. Returns empty string if the URL does not exist.
     *
     * @return string
     */
    public function getIframeUrl(): string
    {
        $result = '';

        try {
            $result = $this->session->getIframeUrl();
        } catch (SessionDataException $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Returns the payment session ID. Returns empty string if it doesn't exist.
     *
     * @return string
     */
    public function getPaymentSessionId(): string
    {
        $result = '';

        try {
            $result = $this->session->getPaymentSessionId();
        } catch (SessionDataException $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Returns the active client API username.
     *
     * @return string
     */
    public function getActiveUsername(): string
    {
        $result = '';

        try {
            $result = $this->coreConfig->getUsername(
                $this->storeManager->getStore()->getCode()
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Returns the selected client API environment.
     *
     * @return int
     */
    public function getEnvironment(): int
    {
        $result = 0;

        try {
            $result = $this->coreConfig->getEnvironment(
                $this->storeManager->getStore()->getCode()
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Check if session is initialized.
     *
     * @return bool
     */
    public function isInitialized(): bool
    {
        $isInitialized = $this->session->isInitialized();

        if (!$isInitialized) {
            $this->log->error(
                'Checkout process will fail. ' .
                'Payment session has not been initialized.'
            );
        }

        return $isInitialized;
    }

    /**
     * Check if the Alert Shipping Method Updates setting is active.
     *
     * @return bool
     */
    public function isAlertShippingMethodUpdates(): bool
    {
        $result = false;

        try {
            $result = $this->config->isAlertShippingMethodUpdates(
                $this->storeManager->getStore()->getCode()
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
